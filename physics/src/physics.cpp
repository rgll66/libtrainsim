#include "physics.hpp"

using namespace libtrainsim;
using namespace libtrainsim::core;

using namespace sakurajin::unit_system::base::literals;
using namespace sakurajin::unit_system::common::literals;
using namespace sakurajin::unit_system;

physics::physics(const Track& conf, bool _autoTick):config(conf),autoTick(_autoTick){

    std::scoped_lock<std::shared_mutex> lock1(mutex_data);
    velocity = 0.0_mps;
    location = config.firstLocation();
    acceleration = 0.0_mps2;

    last_update = libtrainsim::core::Helper::now();

    std::scoped_lock<std::shared_mutex> lock2(mutex_error);
    hasError = false;
    return;
};

physics::~physics(){

};

void physics::emergencyBreak(){
    std::scoped_lock<std::shared_mutex> lock(mutex_data);
    isEmergencyBreaking = true;
}

common::speed physics::getVelocity(){
    if(autoTick){tick();};
    std::shared_lock<std::shared_mutex> lock(mutex_data);
    return velocity;
}

base::length physics::getLocation(){
    if(autoTick){tick();};
    std::shared_lock<std::shared_mutex> lock(mutex_data);
    return location;
}

common::acceleration physics::getAcceleration(){
    if(autoTick){tick();};
      std::shared_lock<std::shared_mutex> lock(mutex_data);
      return acceleration;
}

void physics::setSpeedlevel(core::input_axis slvl){
    tick();
    std::scoped_lock<std::shared_mutex> lock(mutex_data);
    speedlevel = slvl.get();
}

common::force physics::getTraction(){
    if(autoTick){tick();};
    std::shared_lock<std::shared_mutex> lock(mutex_data);
    return currTraction;
}

common::power physics::getCurrPower(){
  if(autoTick){tick();};
  std::shared_lock<std::shared_mutex> lock(mutex_data);
  return currPower;
}

bool physics::isValid(){
    std::shared_lock<std::shared_mutex> lock(mutex_error);
    return !hasError;
}

bool physics::reachedEnd(){
    if(autoTick){tick();};
    std::scoped_lock<std::shared_mutex> lock(mutex_data);
    return std::abs((location - config.lastLocation()).value) < 0.1;
}

sakurajin::unit_system::common::force physics::calcDrag(){
    std::shared_lock<std::shared_mutex> lock(mutex_data);

    auto totalDrag = 0_N;

    //calculating the current air drag acting on the train, depending on the train being in a tunnel or not 
    auto velocityWind = currentWindVelocity;
    auto currentVelocity = velocity;

    //variablen erstezen mit Train.get
    auto surfaceTrain = 10.4;
    auto trainType = Physics::passenger;
    auto mass = config.train().getMass();
    auto numberWagons = 5;
    auto driverLength = 20_m;
    auto totalTrainLength = 5 * 25_m + driverLength;

    //0.0 ersetzen mit Track.at (length)
    double trackRadius = 0.0;
    double incline = 0.0;
    double trackFrictionMultiplier = 1.0;
    double surfaceTunnel = M_PI * pow( 3.25 , 2 );;
    auto lengthTunnel = 0_m;
    auto currentEnvironment = Physics::outside;
    
    lock.unlock();

    double blockingDimension =  surfaceTrain / surfaceTunnel;

    //convert the tunnel length to meter
    lengthTunnel = unit_cast(lengthTunnel, 1.0);

    auto lengthWagon = totalTrainLength - driverLength;

    currentVelocity = unit_cast(currentVelocity,(1_kmph).multiplier);
    //calculate air drag depending on tain type and environment
    if(trainType == Physics::passenger && currentEnvironment == Physics::outside){
        //hier einheiten korrigeiren
        double airDrag = 1.9 + 0.0025 * currentVelocity.value + 0.48*((numberWagons+2.7)/mass.value) * surfaceTrain*pow((currentVelocity+velocityWind).value,2);
        totalDrag += airDrag * mass * 0.001 * 1_G;
    }else if(trainType ==  Physics::passenger && currentEnvironment == Physics::tunnel){ 
        //hier einheiten korrigeiren
        double airDrag = 0.4;
        airDrag += 0.0048 * (numberWagons + 2.7) * 1.45 * (pow(currentVelocity.value, 2) + (0.4943 *  pow(lengthTunnel.value , 0.05654) + 0.145193 * exp(-0.7385)) * pow(currentVelocity.value * (0.1444/blockingDimension),2.06));
        totalDrag += airDrag * 1_kg * 1_G;
    }else if(trainType == Physics::cargo && currentEnvironment == Physics::outside){
        //hier einheiten korrigeiren
        
        double airDrag = 1.4 * (0.007 + 0.050) * pow((velocity.value/10.0),2);
        totalDrag += airDrag * 1_N;
    }else{
        
        double airDrag = (0.07+0.050) * (pow(currentVelocity.value,2)/10)  + 0.4 * pow(currentVelocity.value,2) + 0.371 * 0.0721179 * (5.9508 * pow(lengthTunnel.value , 0.4055058) + 12.1861/lengthTunnel.value) * (((lengthWagon).value / 182.918) + 0.4 ) * pow(pow(currentVelocity.value*(0.1444 / blockingDimension) , 0.213 ) , 2.06);
        totalDrag = airDrag * mass * 1_G;
    }

    //calculating the friction for the track if the train currently is on a arc section(rail curve), disabled for libtrainsim::core::Helper::now() since the required track data isnt available yet  
    
    if(trackRadius >= 300 && trackRadius <= 1000){
        double arcFrition = 650 /(trackRadius-55);
        totalDrag += arcFrition * 1_N;
    }else if (trackRadius < 300){
        double arcFrition = 500 /(trackRadius-55);
        totalDrag += arcFrition * 1_N;
    }
    
    //since the coefficient of static friction is also dependent on the current velocity of the train, its non constant component has to be calculated dynamically 
    
    totalDrag += ((7.5/(currentVelocity.value+44))+0.161) * 1_N;

    //acceleration also effects the current on the train acting friction, this is calculated in the following section(the massFactor is depending on train type, for libtrainsim::core::Helper::now() a constant value of 1.08 is being used):
    double massFactor = 1.08;
    totalDrag += ((acceleration * massFactor ) / 1_G) * 1_N;

    //calculating the ammount of friction caused by inclination and different train designs(also missing the inclination data)
    totalDrag += trackFrictionMultiplier * baseTrackFriction * cos(incline) * mass * 1_G;


    return totalDrag;

}

void physics::updateBreakTemp(){    

    //calculating current brake temperature, currently assuming an temperature of the surrounding of 20 degrees celsius
    float elevationBefore = 0;
    float elevationAfter = 0; 
    int temperatureSurroundings = 20;
    int numberBrakes = 10;
    float differenceElevation;

    if (elevationBefore > elevationAfter){
        differenceElevation = elevationBefore - elevationAfter;
    }else{
        differenceElevation = 0;
    }

    //brakeParameter = numberBrakes * specificHeatCapacityBrake * massBrake(currently set as constant too, due to missing data);
    float brakeParameter = numberBrakes * 460 * 86;
    double areaConvection = 1.724;
    int massBrake = 86;
    double d = (50 * areaConvection) / (460 * massBrake);
    double timeTotal = 0;
    //timeTotal += new_time;
    //double temperatureBrakes = temperatureSurroundings + ((mass * g * differenceElevation + 0.5 * mass * pow(velocity,2) ) / brakeParameter) - temperatureSurroundings) * exp(d * timeTotal);
    //assuming the lowest temperature the brakes can reach is the temperature of the surroundings and the highest being the melting point of the used material
    //temperatureBrakes = clamp(temperatureBrakes,temperatureSurroundings,1150);

}

void physics::tick(){

    auto totalFriction = calcDrag();
    updateBreakTemp();

    std::scoped_lock<std::shared_mutex> lock(mutex_data);

    auto new_time = libtrainsim::core::Helper::now();

    base::time_si dt = unit_cast(new_time - last_update);

    //get variables from train
    auto mass = config.train().getMass();
    auto MaxPower = config.train().getMaxPower();

    //calculate some more vars
    if(isEmergencyBreaking){
        speedlevel = -1.0;
        if(velocity < 0.007_mps){
          isEmergencyBreaking = false;
        }
    }
    currPower = speedlevel*MaxPower;

    //Handling the different possibilities for Speedlevel
    //only calculating the current Force
    if(speedlevel < 0.007 && speedlevel > -0.007){
        currTraction = 0_N;
        if (velocity > 0.0_mps){
          currTraction = currPower/velocity;
        }
    }else if(speedlevel < 0){
        currTraction = speedlevel*totalFriction;
    }else{
        if (abs(velocity) < 0.07_mps){
            currTraction = totalFriction;
        }else{
            currTraction = currPower/velocity-totalFriction;
        }
        
    }
    
    currTraction = clamp(currTraction,-totalFriction,totalFriction);
    
    //calculating parameters of movement by current Traction
    acceleration = currTraction/mass;
   
    velocity += acceleration * dt;
    velocity = clamp(velocity,0_mps,10000_mps);
    location += velocity * dt + 0.5 * (acceleration * dt * dt);
    location = clamp(location, config.firstLocation(),config.lastLocation());

    last_update = new_time;

    return;
}
